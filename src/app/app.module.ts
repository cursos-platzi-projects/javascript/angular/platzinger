import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ConversationComponent } from './conversation/conversation.component';
import { ProfileComponent } from './profile/profile.component';
import { Routes, RouterModule } from '@angular/router';
import { MenuComponent } from './menu/menu.component';
import { SearchPipe } from './pipes/search';
import { FormsModule } from '@angular/forms';

import {AngularFireModule} from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from 'src/environments/environment';
import { AuthenticationGuard } from './services/authentication.guard';

import { ImageCropperModule } from 'ngx-image-cropper';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { RequestComponent } from './modals/request/request.component';

//appRoutes = seran las rutas de nuestra aplicacion
const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent, canActivate: [AuthenticationGuard]}, //AuthenticationGuard = Regresara true or false para dejar authenticar al usuario
  {path: 'login', component: LoginComponent},
  {path: 'conversation/:uid', component: ConversationComponent}, // :uid = agregando parametro en la url
  {path: 'profile', component: ProfileComponent, canActivate: [AuthenticationGuard]}
];
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ConversationComponent,
    ProfileComponent,
    MenuComponent,
    SearchPipe,
    RequestComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes), //abilita el ruteo de nuestra aplicacion
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
    AngularFireDatabaseModule, //from firebase
    ImageCropperModule, //Para adaptar el tamano de las imagenes
    NgbModule, // Para Bootstrap
    BootstrapModalModule.forRoot({container: document.body}) // Para hacer modales mucho mas sencillos
    //container: document.body = Para que los modales puedan estar en cualquier lado de la aplicacion

   // AppRoutingModule 
  ],
  providers: [],
  bootstrap: [AppComponent],
  //entryComponents = para cuando llamemos al RequestComponent este listo para ser usado
  entryComponents: [RequestComponent]
})
export class AppModule { }
