import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { AuthenticationService } from '../services/authentication.service';
import { User } from '../interfaces/user';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user: User;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  picture: any;
  constructor(private userService: UserService,
              private authenticationService: AuthenticationService,
              private firebaseStorage: AngularFireStorage) { 
                this.authenticationService.getStatus().subscribe((status) => {
                  this.userService.getUserById(status.uid).valueChanges().subscribe((data: User) => {
                      this.user = data;
                      console.log(this.user);
                  }, (error) => {
                    console.log(error);
                  });
                }, (error) => {
                  console.log(error);
                });
              }

  saveSettings() {
    if(this.croppedImage) { // Checar si ya hay una imagen recortada
      const currentPictureId = Date.now(); // Crear nombre para esa imagen
      //El path de nuestro storage
      const pictures = this.firebaseStorage.ref('pictures/' + currentPictureId + '.jpg').putString(this.croppedImage, 'data_url'); //putString = subimos la imagen la cual se convirtio a binaria

      pictures.then((result) => {
        //Cuando se haya subido, obtener la url de esa imagen binaria
        this.picture = this.firebaseStorage.ref('pictures/' + currentPictureId + '.jpg').getDownloadURL();

        //Para hacer la accion de obtener la url, p = url de la imagen
        this.picture.subscribe((url_image) => {
          this.userService.setAvatar(url_image, this.user.uid).then(() => {
            alert('Avatar subido correctamente');
          }).catch((error) => {
            alert('Hubo un error al subir la imagen');
            console.log(error);
          });
        });
      }).catch((error) => {
        console.log(error);
      });
    }
    else {
      this.userService.editUser(this.user).then(() => {
        alert("Cambios guardados");
      }).catch((error) => {
        alert("Hubo un error");
        console.log(error);
      })
    }
    
  }
  
  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
      this.croppedImage = event.base64;
  }
  imageLoaded() {
      // show cropper
  }
  cropperReady() {
      // cropper ready
  }
  loadImageFailed() {
      // show message
  }

  ngOnInit() {
  }

}
