import { Injectable } from '@angular/core';
import { User } from '../interfaces/user';
import { AngularFireDatabase } from '@angular/fire/database';

/** SERVICE PARA LOS USUARIOS */

@Injectable({
  providedIn: 'root' // Con esto significa que el componente puede estar disponible en toda la aplicacion
})
export class UserService {
  constructor(private angularFireDatabase: AngularFireDatabase) { }

  //Obtiene la lista de usuarios que estan registrados en la Database
  getUsers() {
    return this.angularFireDatabase.list('/users');
  }

  //Obtiene los datos del usuario en la Database
  getUserById(uid) {
    return this.angularFireDatabase.object('/users/' + uid);
  }

  //Agrega los datos del usuario en la Database
  createUser(user) {
    return this.angularFireDatabase.object('/users/' + user.uid).set(user);
  }

  //Edita los datos del usuario en la Database
  editUser(user) {
    return this.angularFireDatabase.object('/users/' + user.uid).set(user);
  }

  //Guarda imagen del usuario al Firebase
  setAvatar(avatar, uid) {
    return this.angularFireDatabase.object('/users/' + uid + '/avatar').set(avatar);
  }

  addFriend(userId, friendId) {
    this.angularFireDatabase.object('users/' + userId + 'friends/' + friendId).set(friendId);
    return this.angularFireDatabase.object('users/' + friendId + '/friends/' + userId).set(userId);
  }

}

/*
Los servicios te ayudan para obtener datos de algun origen de datos.
Basicamente es como el Modelo en MVC.
*/