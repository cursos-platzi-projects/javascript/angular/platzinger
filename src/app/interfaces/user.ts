export interface User {
    nick: string;
    subnick: string;
    age: number;
    email?: string;
    friend: boolean;
    uid: any;
    status?: string;
    avatar?: string;
}

/*
Typescript es muy estricto de tal forma que cuando declaros nuestro
objeto de tipo User, debemos asignarle un valor a cada propiedad
declara en la interfaz, de lo contrario te marcara error si omites
una propiedad, a menos que le pongas al final de la propiedad el 
simbolo ? para que no sea obligatorio setear un valor
*/