import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'search' // --- este es el nombre con que se implementa en el html
})
export class SearchPipe implements PipeTransform {
    //value = biene siendo nuestro arreglo'
    //args = la palabra de busqueda
    public transform(value, args: string) {
        if(!value) {
            return;
        }
        if(!args) {
            return value;
        }
        args = args.toLocaleLowerCase();
        return value.filter( (item) => {
            return JSON.stringify(item).toLocaleLowerCase().includes(args);
        })
    }
}

/*
includes = lo que hace es encontrar la palabra que le esta pasando por el arreglo,
y asi saber si se encontro
*/