import { Component, OnInit } from '@angular/core';
import { User } from '../interfaces/user';
import { UserService } from '../services/user.service';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { RequestsService } from '../services/requests.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  friends: User[];
  query: string = '';
  friendEmail: string = '';
  user: User;
  constructor(private userService: UserService,
              private authenticationService: AuthenticationService,
              private router: Router,
              private modalService: NgbModal,
              private requestService: RequestsService) {
    //valueChanges = cada que cambie el valor que traiga en getUsers(), se va ejecutar en el valueChanges()
    this.userService.getUsers().valueChanges().subscribe((data: User[]) => {
      this.friends = data;
    }, (error) => {
      console.log(error);
      //Aqui no es recomendable poner alerts
    });

    this.authenticationService.getStatus().subscribe((status) => {
      this.userService.getUserById(status.uid).valueChanges().subscribe((data: User) => {
        this.user = data;
      });
    });
  }

  logout() {
    this.authenticationService.logOut().then(() => {
      alert('Session cerrara');
      this.router.navigate(['login']);
    }).catch((error) => {
      console.log(error);
    });
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      //this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  sendRequest() {
    const request = {
      timestamp: Date.now(),
      receiver_email: this.friendEmail,
      sender: this.user.uid,
      status: 'pending'
    }
    this.requestService.createRequest(request).then(() => {
      alert('Solicitud enviada');
    }).catch((error) => {
      alert('Hubo un error al enviar solicitud');
      console.log(error);
    })
  }

  ngOnInit() {
  }

}

/*
***** Tipos de datos *****
//Esto se puede hacer pero no deberia por el rendimiento
let c = 1;
let b - "2"

//Esto es lo correcto
let c: number = 1;
let b: number = 2;

let e: string = '1';
let f: string = '2';

let g: boolean = true;
let h: object = {}

let i: [c, b, e, f, g, h];

let j: = boolean [] = [true, g];
let k: object[] = [{}, h];
let l: any[] = [1, 'aoe', {}, []]

//De esta manera se pueden detectar errores, por ejemplo si a un string
se le quiere asignar un numero.
*/