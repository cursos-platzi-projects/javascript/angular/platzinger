# Platzinger

The first project I created when I watched the course about Angular.

This project seems like a messenger from desktop



This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.2.

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

-------------

# Documentation Course

`ng new name` = Create new project

`ng serve --open` = para correr nuestra aplicacion, si tiene `--open` = significa que aparte de levantar el servidor tambien nos habra nuestra aplicacion.

`ng generate component nombre` = Para generar un nuevo componente

- Para generar un nuevo servicio

`ng generate service rutaCarpeta` - comando

`ng generate service services/user` - ejemploComando

- Instalando cosas extras a nuestra aplicacion usando NPM

- instalando Bootstrap

`npm install bootstrap --save` = El `--save` lo que hace es guardar la libreria bootstrap en nuestro package.json

`npm install bootstrap --save-exact` = El `--save-exact` para instalar una version exacta de bootstrap

`npm install @fortawesome/fontawesome-free --save-exact`

y despues importamos esas librerias en un archivo la cual es: angular.json

`ng generate guard services/authentication` = Para crear un guard

- Instalando cosas extras a nuestra aplicacion usando NPM

`NgClass` es una directiva que te permite aplicar una u otra clase a un elemento de html, dependiendo de una condición o expresión buleana.
La forma de implementar NgClass es la siguiente:

    <div [ngClass] = ""{ '<nombre-de-la-clase': <expresión buleana> }"">
        <!-- -->
    </div>

- Firebase

Los guards son scripts que implementan una estrategia de seguridad para accesos no autorizados a las diferentes rutas de nuestra aplicación. Se crean de manera similar a los servicios y componentes, con el siguiente comando de AngularCLI:

`ng generate guard <directorio>/<nombre-del-guard>`

Resultando en la creación de los archivos: `<nombre-del-guard>.specs.guard.ts` y `<nombre-del-guard>.guard.ts`

El guard se basa en un atributo llamado canActivate que, dependiendo de una condición o expresión buleana, retornará verdadero o falso al constructor del componente en el que se haya inyectado para indicarle cuando deberá mostrar o no el contenido de dicho componente.

